FROM golang:alpine
LABEL maintainer="passingbreeze"
WORKDIR /slackbot
COPY . .
RUN go build -o ./slack_bot ./main.go
ARG OPENWEATHER_API_KEY
ARG KOREANWEATHER_API_KEY
ARG SLACK_BOT_TOKEN
EXPOSE 8080
ENTRYPOINT [ "./slack_bot" ]
