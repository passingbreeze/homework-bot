package controllers

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/slack-go/slack"
)

func TestGetWeatherInfo(t *testing.T) {
	// owurl := "https://api.openweathermap.org/data/2.5/weather?lat=37.5666791&lon=126.9782914&appid=" + os.Getenv("OPENWEATHER_API_KEY")
	korurl := "http://apis.data.go.kr/1360000/VilageFcstInfoService_2.0/getUltraSrtNcst?dataType=JSON&base_date=20221226&base_time=0800&nx=60&ny=127&serviceKey=" + os.Getenv("KOREANWEATHER_API_KEY")
	_, err := GetWeatherInfo(korurl)
	if err != nil {
		t.Errorf("Error from GetWeatherInfo")
	}
}

func TestDecideHomeWork(t *testing.T) {
	now := time.Now()
	date := strconv.Itoa(now.Year()) + strconv.Itoa(int(now.Month())) + strconv.Itoa(now.Day()-1)
	baseDate := "&base_date=" + date
	baseTime := "&base_time=" + "0800"
	korurl := "http://apis.data.go.kr/1360000/VilageFcstInfoService_2.0/getUltraSrtNcst?serviceKey=" + os.Getenv("KOREANWEATHER_API_KEY") + baseDate + baseTime + "&dataType=JSON&nx=60&ny=127"
	_, err := GetWeatherInfo(korurl)
	if err != nil {
		fmt.Println(korurl)
		t.Errorf("Error from DecideHomework Test : %s\n", err)
	}
}

func TestSlackSendMessage(t *testing.T) {
	SLACK_TOKEN := os.Getenv("SLACK_BOT_TOKEN")
	CHANNEL_ID := os.Getenv("SLACK_BOT_TEST_CHANNEL_ID")
	api := slack.New(SLACK_TOKEN)

	attachment := slack.Attachment{
		Pretext: "✨ _테스트 메시지_",
		Text:    "GitLab Test에서 인사드립니다! 👋",
	}

	channelId, timestamp, err := api.PostMessage(
		CHANNEL_ID,
		slack.MsgOptionText("*Homework-bot의 Test가 모두 통과되었습니다.* 🎉", false),
		slack.MsgOptionAttachments(attachment),
		slack.MsgOptionAsUser(true),
	)

	if err != nil {
		log.Fatalf("%s\n", err)
	}

	log.Printf("Message successfully sent to Channel %s at %s\n", channelId, timestamp)
}
