package controllers

import (
	"strings"

	homebot_ifcs "gitlab.com/passingbreeze/homework-bot/src/interfaces"
	"gitlab.com/passingbreeze/homework-bot/src/models"
)

func GetWeatherInfo(url string) (homebot_ifcs.WeatherInfo, error) {
	var wr homebot_ifcs.WeatherInfo
	var err error
	if strings.Contains(url, "openweathermap.org") {
		wr, err = models.NewOwmResp(url)
	} else {
		wr, err = models.NewKorResp(url)
	}

	if err != nil {
		return nil, err
	}
	return wr, nil
}

func DecideHomeWork(dc homebot_ifcs.WeatherInfo) bool {
	temp, _ := dc.GetFeelLikeTemp()
	snow, _ := dc.GetSnowVolume()
	return temp <= -10.0 && snow >= 1.0
}
