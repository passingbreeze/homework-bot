package interfaces

type WeatherInfo interface {
	GetFeelLikeTemp() (float64, error)
	GetSnowVolume() (float64, error)
}
