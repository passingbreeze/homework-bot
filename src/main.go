package main

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/aws/aws-lambda-go/events"
	runtime "github.com/aws/aws-lambda-go/lambda"
	"github.com/emirpasic/gods/sets/hashset"
	jsoniter "github.com/json-iterator/go"
	wctrl "gitlab.com/passingbreeze/homework-bot/src/controllers"
)

func main() {
	runtime.Start(Handler)
}

func Handler(ctx context.Context, evnts events.CloudWatchEvent) (int, error) {
	DecideHome := hashset.New()

	eventJson, jsonErr := jsoniter.MarshalIndent(evnts, "", "    ")
	if jsonErr != nil {
		log.Fatalln(jsonErr)
	}
	log.Printf("%s\n", eventJson)

	urls := make([]string, 4)
	urls = append(urls, GetOpenWeatherURLs()...)
	urls = append(urls, GetKoreaWeatherURLs()...)

	var wg sync.WaitGroup
	wg.Add(len(urls))
	for _, u := range urls {
		url := u
		go func() {
			defer wg.Done()
			wr, isErr := wctrl.GetWeatherInfo(url)
			if isErr != nil {
				log.Fatalln(isErr)
			}
			DecideHome.Add(wctrl.DecideHomeWork(wr))
		}()
	}
	wg.Wait()

	if DecideHome.Contains(false) {
		fmt.Println("출근하시죠")
	} else if DecideHome.Contains(true) {
		fmt.Println("재택을 권합니다")
	} else {
		return 1, fmt.Errorf("error from system")
	}
	return 0, nil
}

func GetOpenWeatherURLs() []string {
	const Seoul_lat, Seoul_lon = "37.5666791", "126.9782914"
	const Pangyo_lat, Pangyo_lon = "37.3899495", "127.0985928"
	weatherURL := url.URL{
		Scheme:   "https",
		RawPath:  "api.openweathermap.org/data/2.5/weather",
		RawQuery: "units=metric&appid=" + os.Getenv("OPENWEATHER_API_KEY"),
	}

	seoulURL := weatherURL
	seoulURL.Query().Add("lat", Seoul_lat)
	seoulURL.Query().Add("lon", Seoul_lon)

	pangyoURL := weatherURL
	pangyoURL.Query().Add("lat", Pangyo_lat)
	pangyoURL.Query().Add("lon", Pangyo_lon)

	return []string{
		seoulURL.String(),
		pangyoURL.String(),
	}

}

func GetKoreaWeatherURLs() []string {
	const Seoul_x, Seoul_y = "60", "127"
	const Pangyo_x, Pangyo_y = "62", "123"
	now := time.Now()
	date := strconv.Itoa(now.Year()) + strconv.Itoa(int(now.Month())) + strconv.Itoa(now.Day())
	baseDate := "base_date=" + date
	baseTime := "&base_time=" + "0800"

	weatherURL := url.URL{
		Scheme:   "http",
		RawPath:  "apis.data.go.kr/1360000/VilageFcstInfoService_2.0/getUltraSrtNcst",
		RawQuery: baseDate + baseTime + "&dataType=JSON&serviceKey=" + os.Getenv("KOREANWEATHER_API_KEY"),
	}
	seoulURL := weatherURL
	seoulURL.Query().Add("nx", Seoul_x)
	seoulURL.Query().Add("ny", Seoul_y)

	pangyoURL := weatherURL
	pangyoURL.Query().Add("nx", Pangyo_x)
	pangyoURL.Query().Add("ny", Pangyo_y)

	return []string{
		seoulURL.String(),
		pangyoURL.String(),
	}

}
