package models

import (
	"io"
	"math"
	"net/http"
	"strconv"

	jsoniter "github.com/json-iterator/go"
	homebot_ifcs "gitlab.com/passingbreeze/homework-bot/src/interfaces"
)

type KoreaWeatherResp struct {
	Resp KoreaWeatherRespJson `json:"response"`
}

type KoreaWeatherRespJson struct {
	Body KoreaWeatherRespBody `json:"body"`
}

type KoreaWeatherRespBody struct {
	Items KoreaWeatherRespItems `json:"items"`
}

type KoreaWeatherRespItems struct {
	ItemList []KoreaWeatherRespItem `json:"item"`
}

type KoreaWeatherRespItem struct {
	Category string `json:"category"`
	Value    string `json:"obsrValue"`
}

func NewKorResp(url string) (homebot_ifcs.WeatherInfo, error) {
	wk := &KoreaWeatherResp{}
	resp, respErr := http.Get(url)
	if respErr != nil {
		return nil, respErr
	}
	defer resp.Body.Close()

	data, jsonParseErr := io.ReadAll(resp.Body)
	if jsonParseErr != nil {
		return nil, jsonParseErr
	}
	toJsonErr := jsoniter.Unmarshal(data, wk)
	if toJsonErr != nil {
		return nil, toJsonErr
	}
	return wk, nil
}

func (wk *KoreaWeatherResp) GetFeelLikeTemp() (float64, error) {
	var Temp float64
	var WindSpeed float64
	for _, item := range wk.Resp.Body.Items.ItemList {
		switch item.Category {
		case "T1H":
			temp, err := strconv.ParseFloat(item.Value, 64)
			if err != nil {
				return 0.0, err
			}
			Temp = temp
		case "WSD":
			spd, err := strconv.ParseFloat(item.Value, 64)
			if err != nil {
				return 0.0, err
			}
			WindSpeed = spd
		}
	}
	if Temp > -10 {
		return Temp, nil
	} else {
		WindSpeed = 3600.0 * WindSpeed / 1000.0
		return 13.12 + 0.6125*Temp - 11.37*math.Pow(WindSpeed, 0.16) + 0.3965*math.Pow(WindSpeed, 0.16)*Temp, nil
		// formula come from https://data.kma.go.kr/climate/windChill/selectWindChillChart.do?pgmNo=111
	}
}

func (wk *KoreaWeatherResp) GetSnowVolume() (float64, error) {
	var Snowy float64
	for _, item := range wk.Resp.Body.Items.ItemList {
		switch item.Category {
		case "RN1":
			snow, err := strconv.ParseFloat(item.Value, 64)
			if err != nil {
				return 0.0, err
			}
			Snowy = snow / 0.75
		}
	}
	return Snowy, nil
}
