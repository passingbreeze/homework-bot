package models

import (
	"io"
	"net/http"

	jsoniter "github.com/json-iterator/go"
	homebot_ifcs "gitlab.com/passingbreeze/homework-bot/src/interfaces"
)

type OpenWeatherMapMain struct {
	Feels_like float64 `json:"feels_like"`
}

type OpenWeatherMapSnow struct {
	OneHour float64 `json:"1h"`
}
type OpenWeatherMapResp struct {
	Main OpenWeatherMapMain `json:"main"`
	Snow OpenWeatherMapSnow `json:"snow"`
}

func NewOwmResp(url string) (homebot_ifcs.WeatherInfo, error) {
	wr := &OpenWeatherMapResp{}
	resp, respErr := http.Get(url)
	if respErr != nil {
		return nil, respErr
	}
	defer resp.Body.Close()

	data, jsonParseErr := io.ReadAll(resp.Body)
	if jsonParseErr != nil {
		return nil, jsonParseErr
	}
	toJsonErr := jsoniter.Unmarshal(data, wr)
	if toJsonErr != nil {
		return nil, toJsonErr
	}
	return wr, nil
}

func (wr *OpenWeatherMapResp) GetFeelLikeTemp() (float64, error) {
	return wr.Main.Feels_like, nil
}

func (wr *OpenWeatherMapResp) GetSnowVolume() (float64, error) {
	return wr.Snow.OneHour, nil
}
