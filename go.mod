module gitlab.com/passingbreeze/homework-bot

go 1.19

require (
	github.com/aws/aws-lambda-go v1.36.1
	github.com/emirpasic/gods v1.18.1
	github.com/json-iterator/go v1.1.12
	github.com/slack-go/slack v0.12.1
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
)
